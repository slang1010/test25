//
//  gv_AppDelegate.h
//  test25
//
//  Created by Sergiu on 28.12.13.
//  Copyright (c) 2013 Sergiu Langa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface gv_AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
