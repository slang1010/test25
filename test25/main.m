//
//  main.m
//  test25
//
//  Created by Sergiu on 28.12.13.
//  Copyright (c) 2013 Sergiu Langa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "gv_AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([gv_AppDelegate class]));
    }
}
